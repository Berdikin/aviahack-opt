from flask import Flask, request, jsonify
from scipy.optimize import milp, LinearConstraint
import numpy as np

app = Flask(__name__)


@app.route('/', methods=['POST'])
def optimize():
    input_json = request.get_json(force=True)
    vehicle = input_json['vehicle']
    count_of_vehicles = len(vehicle)

    ids = [vehicle[i]['id'] for i in range(count_of_vehicles)]
    durations = [vehicle[i]['duration'] for i in range(count_of_vehicles)]
    capacities = [vehicle[i]['capacity'] for i in range(count_of_vehicles)]
    requirement_capacity = input_json['requirement_capacity']

    c = np.array(durations)
    A = np.array(capacities)
    b_l = np.array(requirement_capacity)

    constraints = LinearConstraint(A, b_l)
    integrality = np.ones_like(c)

    res = milp(c=c, constraints=constraints, integrality=integrality, bounds=[0, 1])
    print([int(e) for e in res.x])

    result_ids = []
    for i in range(len(ids)):
        if int(res.x[i]) == 1:
            result_ids.append(ids[i])

    return jsonify(result_ids)


if __name__ == '__main__':
    app.run()
