from app import app
import json


class TestOptimization:

    def setup(self):
        app.testing = True
        self.client = app.test_client()

    def test_optimize_full(self):
        response = self.client.post('/', data=json.dumps({
            "requirement_capacity": 100, "vehicle": [
                {
                    "id": 1,
                    "duration": 10,
                    "capacity": 20
                },
                {
                    "id": 2,
                    "duration": 20,
                    "capacity": 30
                },
                {
                    "id": 3,
                    "duration": 25,
                    "capacity": 50
                }
            ]}))
        assert response.status_code == 200
        assert response.json == [1, 2, 3]

    def test_optimize_simple(self):
        response = self.client.post('/', data=json.dumps({
            "requirement_capacity": 15, "vehicle": [
                {
                    "id": 1,
                    "duration": 50,
                    "capacity": 20
                },
                {
                    "id": 2,
                    "duration": 10,
                    "capacity": 30
                },
                {
                    "id": 3,
                    "duration": 40,
                    "capacity": 50
                }
            ]}))
        assert response.status_code == 200
        assert response.json == [2]

    def test_optimize_simple_2(self):
        response = self.client.post('/', data=json.dumps({
            "requirement_capacity": 15, "vehicle": [
                {
                    "id": 1,
                    "duration": 5,
                    "capacity": 20
                },
                {
                    "id": 2,
                    "duration": 10,
                    "capacity": 30
                },
                {
                    "id": 3,
                    "duration": 40,
                    "capacity": 50
                }
            ]}))
        assert response.status_code == 200
        assert response.json == [1]

    def test_optimize_simple_3(self):
        response = self.client.post('/', data=json.dumps({
            "requirement_capacity": 45, "vehicle": [
                {
                    "id": 1,
                    "duration": 5,
                    "capacity": 30
                },
                {
                    "id": 2,
                    "duration": 10,
                    "capacity": 16
                },
                {
                    "id": 3,
                    "duration": 40,
                    "capacity": 50
                }
            ]}))
        assert response.status_code == 200
        assert response.json == [1, 2]
